﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Breakthrough.Data;
using UnityEngine;
using Random = System.Random;

namespace Breakthrough.State
{
    public sealed class FactoryState
    {
        public List<ProductState> ProductStates;
        public Dictionary<string, SectorState> SectorStates;

        public FactoryState(FakeData data)
        {
            ProductStates = data.Factory.Products.Select(x => new ProductState(x)).ToList();
            SectorStates = new Dictionary<string, SectorState>();
        }

        public void StartWork()
        {
            foreach (var productState in ProductStates)
            {
                productState.StartWork();
            }
        }

        public void Update()
        {
            foreach (var productState in ProductStates)
            {
                productState.Update();
            }
        }
    }

    public sealed class ProductState
    {
        public ProductStateType Type;
        public string Sector;
        public List<string> HistorySectors;
        public List<PartState> PartStates;

        public event Action<ProductStateType, ProductInfo> StateChanged;

        private float _routeTime;

        private ProductInfo _productInfo;
        public ProductInfo ProductInfo { get { return _productInfo; } }

        public ProductState(ProductInfo productInfo)
        {
            _productInfo = productInfo;
            Type = ProductStateType.Queue;
            PartStates = productInfo.Parts.Select(x => new PartState(x)).ToList();
        }

        public void StartWork()
        {
            Type = ProductStateType.Work;
            if (StateChanged != null)
            {
                StateChanged(Type, _productInfo);
            }

            foreach (var partState in PartStates)
            {
                partState.StartWork();
            }

            _routeTime = UnityEngine.Random.Range(5, 8);
        }

        public void Update()
        {
            if (Type == ProductStateType.Storage)
            {
                return;
            }

            if (Type == ProductStateType.Route)
            {
                _routeTime -= Time.deltaTime;
                if (_routeTime < 0)
                {
                    Type = ProductStateType.Storage;
                    if (StateChanged != null)
                    {
                        StateChanged(Type, _productInfo);
                    }
                }
                return;
            }
            PartStates.ForEach(x => x.Update());
            if (PartStates.All(x => x.Type == PartStateType.Storage))
            {
                Type = ProductStateType.Route;
                if (StateChanged != null)
                {
                    StateChanged(Type, _productInfo);
                }
            }
        }
    }

    public enum ProductStateType
    {
        Queue = 0,
        Work = 1,
        Route = 2,
        Storage = 3
    }

    public sealed class PartState
    {
        public PartStateType Type;
        public string Sector;
        public List<string> HistorySectors;

        public event Action<PartStateType, ProductPartInfo> StateChanged;

        private ProductPartInfo _productPartInfo;
        public ProductPartInfo ProductPartInfo { get { return _productPartInfo; } }

        private float _routeTime;
        private float _buildingTime;

        public PartState(ProductPartInfo productPartInfo)
        {
            _productPartInfo = productPartInfo;

            Type = PartStateType.Wait;
            Sector = string.Empty;
            HistorySectors = new List<string>();
        }

        public void StartWork()
        {
            Type = PartStateType.Work;
            if (StateChanged != null)
            {
                StateChanged(Type, _productPartInfo);
            }

            _routeTime = UnityEngine.Random.Range(5, 8);
            _buildingTime = UnityEngine.Random.Range(5, 8);
        }

        public void Update()
        {
            if (Type == PartStateType.Work)
            {
                _buildingTime -= Time.deltaTime;
                if (_buildingTime < 0)
                {
                    Type = PartStateType.Route;
                    if (StateChanged != null)
                    {
                        StateChanged(Type, _productPartInfo);
                    }
                }
                return;
            }

            if (Type == PartStateType.Route)
            {
                _routeTime -= Time.deltaTime;
                if (_routeTime < 0)
                {
                    Type = PartStateType.Storage;
                    if (StateChanged != null)
                    {
                        StateChanged(Type, _productPartInfo);
                    }
                }
                return;
            }
        }
    }

    public enum PartStateType
    {
        Wait = 0,
        Work = 1,
        Route = 2,
        Storage = 3
    }

    public sealed class SectorState
    {
        public string StorageId;
        public bool IsBusy;
        public float RemainingTime;
        public int CompletedCount;

        public void Update()
        {
            RemainingTime -= Time.deltaTime;
        }
    }

    public enum SectorType
    {
        Building = 0,
        Storage = 1
    }
}