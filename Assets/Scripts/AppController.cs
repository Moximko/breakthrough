﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Breakthrough.Data;
using Breakthrough.State;
using Breakthrough.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Breakthrough
{
    public class AppController : MonoBehaviour
    {
        public ProductsUI ProductsUI;
        public PartsUI PartsUI;
        public List<Sector> Sectors;
        public Button ChangeSceneButton;

        private FactoryState _factoryState;
        private ProductState _productState;

        private void Start()
        {
            var factoryData = FakeDataHolder.Load();
            if (factoryData == null)
            {
                throw new UnityException("Can't load factory data!");
                return;
            }

            _factoryState = new FactoryState(factoryData);

            ProductsUI.Init(_factoryState);
            ProductsUI.OnProductStateButtonClick += OnProductStateButtonClicked;

            PartsUI.OnBackButtonClick += OnPartsBackButtonClicked;

            _factoryState.StartWork();

            _productState = _factoryState.ProductStates.First();
            ShowSectors(_productState);

            _productState.StateChanged += ProductStateOnStateChanged;
            foreach (var partState in _productState.PartStates)
            {
                partState.StateChanged += PartStateOnStateChanged;
            }
        }

        private void PartStateOnStateChanged(PartStateType arg1, ProductPartInfo arg2)
        {
            if (_productState != null)
            {
                ShowSectors(_productState);
            }
        }

        private void ProductStateOnStateChanged(ProductStateType arg1, ProductInfo arg2)
        {
            if (_productState != null)
            {
                ShowSectors(_productState);
            }
        }

        private void OnProductStateButtonClicked(ProductState productState)
        {
            ProductsUI.gameObject.SetActive(false);
            PartsUI.gameObject.SetActive(true);
            PartsUI.Init(productState);
            ShowSectors(productState);

            if (_productState != null)
            {
                _productState.StateChanged -= ProductStateOnStateChanged;
                foreach (var partState in _productState.PartStates)
                {
                    partState.StateChanged -= PartStateOnStateChanged;
                }
            }

            _productState = productState;
            _productState.StateChanged += ProductStateOnStateChanged;
            foreach (var partState in _productState.PartStates)
            {
                partState.StateChanged += PartStateOnStateChanged;
            }
        }

        private void OnPartsBackButtonClicked()
        {
            ProductsUI.gameObject.SetActive(true);
            PartsUI.gameObject.SetActive(false);
        }

        private void ShowSectors(ProductState productState)
        {
            foreach (var sector in Sectors)
            {
                sector.gameObject.SetActive(false);
                foreach (var partState in productState.PartStates)
                {
                    if (partState.ProductPartInfo.BuildingSectorId == sector.Id)
                    {
                        sector.gameObject.SetActive(true);
                        sector.Show(partState.Type);
                    }
                }

            }
        }

        private void Update()
        {
            _factoryState.Update();
        }

        private void OnDestroy()
        {
            if (_productState == null)
            {
                return;
            }

            _productState.StateChanged -= ProductStateOnStateChanged;
            foreach (var partState in _productState.PartStates)
            {
                partState.StateChanged -= PartStateOnStateChanged;
            }
        }
    }
}