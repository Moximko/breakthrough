﻿using System;
using System.Collections;
using System.Collections.Generic;
using Breakthrough.State;
using UnityEngine;

namespace Breakthrough.UI
{
    public class ProductsUI : MonoBehaviour
    {
        public List<ProductUI> Products;

        public event Action<ProductState> OnProductStateButtonClick; 

        public void Init(FactoryState factoryState)
        {
            foreach (var product in Products)
            {
                product.gameObject.SetActive(false);
            }

            for (int i = 0; i < factoryState.ProductStates.Count; i++)
            {
                var productState = factoryState.ProductStates[i];
                var productUI = Products[i];
                productUI.Init(productState);
                productUI.OnProductStateButtonClick += OnProductStateButtonClicked;
            }
        }

        private void OnProductStateButtonClicked(ProductState productState)
        {
            if (OnProductStateButtonClick != null)
            {
                OnProductStateButtonClick(productState);
            }
        }

        private void OnDestroy()
        {
            OnProductStateButtonClick = null;
        }
    }
}