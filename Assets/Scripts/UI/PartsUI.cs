﻿using System;
using System.Collections;
using System.Collections.Generic;
using Breakthrough.State;
using UnityEngine;
using UnityEngine.UI;

namespace Breakthrough.UI
{
    public class PartsUI : MonoBehaviour
    {
        public Text ProductTitle;
        public Button BackButton;
        public List<PartUI> Parts;

        public event Action OnBackButtonClick;

        public void Init(ProductState productState)
        {
            foreach (var part in Parts)
            {
                part.gameObject.SetActive(false);
            }

            for (int i = 0; i < productState.PartStates.Count; i++)
            {
                var partState = productState.PartStates[i];
                var partUI = Parts[i];
                partUI.Init(partState);
            }

            BackButton.onClick.RemoveAllListeners();
            BackButton.onClick.AddListener(OnBackButtonClicked);

            ProductTitle.text = productState.ProductInfo.DisplayName;
        }

        private void OnBackButtonClicked()
        {
            if (OnBackButtonClick != null)
            {
                OnBackButtonClick();
            }
        }
    }
}