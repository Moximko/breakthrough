﻿using Breakthrough.Data;
using Breakthrough.State;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Breakthrough.UI
{
    public class ProductUI : MonoBehaviour
    {
        public Text TitleText;
        public Text WorkText;
        public Text RouteText;
        public Text StorageText;

        public event Action<ProductState> OnProductStateButtonClick;

        private Text _stateText;
        private ProductState _productState;
        private Button _partsButton;

        public void Init(ProductState productState)
        {
            _stateText = WorkText;

            WorkText.transform.parent.gameObject.SetActive(true);
            RouteText.transform.parent.gameObject.SetActive(false);
            StorageText.transform.parent.gameObject.SetActive(false);

            _productState = productState;
            _productState.StateChanged += OnStateChanged;

            _partsButton = GetComponent<Button>();
            _partsButton.onClick.RemoveAllListeners();
            _partsButton.onClick.AddListener(OnPartsButtonClicked);
            gameObject.SetActive(true);
        }

        public void OnStateChanged(ProductStateType state, ProductInfo productInfo)
        {
            TitleText.text = productInfo.DisplayName;

            _stateText.transform.parent.gameObject.SetActive(false);
            switch (state)
            {
                case ProductStateType.Queue:
                    break;
                case ProductStateType.Work:
                    _stateText = WorkText;
                    break;
                case ProductStateType.Route:
                    _stateText = RouteText;
                    break;
                case ProductStateType.Storage:
                    _stateText = StorageText;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }

            _stateText.text = productInfo.BuildingSectorId;
            _stateText.transform.parent.gameObject.SetActive(true);
        }

        private void OnPartsButtonClicked()
        {
            if (OnProductStateButtonClick != null)
            {
                OnProductStateButtonClick(_productState);
            }
        }

        private void OnDestroy()
        {
            if (_productState != null)
            {
                _productState.StateChanged -= OnStateChanged;
            }

            OnProductStateButtonClick = null;
        }
    }
}