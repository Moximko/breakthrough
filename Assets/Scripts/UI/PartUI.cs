﻿using Breakthrough.Data;
using Breakthrough.State;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Breakthrough.UI
{
    public class PartUI : MonoBehaviour
    {
        public Text TitleText;
        public Text WorkText;
        public Text RouteText;
        public Text StorageText;

        private Text _stateText;
        private PartState _partState;

        public void Init(PartState partState)
        {
            _stateText = WorkText;

            WorkText.transform.parent.gameObject.SetActive(true);
            RouteText.transform.parent.gameObject.SetActive(false);
            StorageText.transform.parent.gameObject.SetActive(false);

            _partState = partState;
            _partState.StateChanged += OnStateChanged;

            OnStateChanged(_partState.Type, _partState.ProductPartInfo);

            gameObject.SetActive(true);
        }

        public void OnStateChanged(PartStateType state, ProductPartInfo partInfo)
        {
            TitleText.text = partInfo.DisplayName;

            _stateText.transform.parent.gameObject.SetActive(false);
            switch (state)
            {
                case PartStateType.Wait:
                    break;
                case PartStateType.Work:
                    _stateText = WorkText;
                    break;
                case PartStateType.Route:
                    _stateText = RouteText;
                    break;
                case PartStateType.Storage:
                    _stateText = StorageText;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }

            _stateText.text = partInfo.BuildingSectorDisplayName;
            _stateText.transform.parent.gameObject.SetActive(true);
        }

        private void OnDisable()
        {
            if (_partState != null)
            {
                _partState.StateChanged -= OnStateChanged;
            }
        }
    }
}