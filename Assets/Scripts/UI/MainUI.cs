﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Breakthrough.UI
{
    public class MainUI : MonoBehaviour
    {
        public Button MenuButton;

        public void OnPrevScene()
        {
            SceneManager.LoadScene("SampleScene");
        }
    }
}