﻿using System;
using System.Collections.Generic;

namespace Breakthrough.Data
{
    [Serializable]
    public sealed class FakeData
    {
        public FactoryData Factory;
    }

    [Serializable]
    public sealed class FactoryData
    {
        public List<ProductInfo> Products;
    }

    [Serializable]
    public sealed class ProductInfo
    {
        public string Id;
        public int Count;
        public string DisplayName;
        public List<ProductPartInfo> Parts;
        public int BuildTime;
        public string BuildingSectorId;
    }

    [Serializable]
    public sealed class ProductPartInfo
    {
        public string Id;
        public string DisplayName;
        public int BuildTime;
        public string BuildingSectorId;
        public string BuildingSectorDisplayName;
    }

    [Serializable]
    public sealed class SectorInfo
    {
        public string Id;
        public string DisplayName;
    }
}