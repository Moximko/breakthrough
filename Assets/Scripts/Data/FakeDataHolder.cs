﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakthrough.Data
{
    [CreateAssetMenu(fileName = "Resources/FactoryFakeData", menuName = "CreateFactoryFakeData")]
    public class FakeDataHolder : ScriptableObject
    {
        public FakeData FactoryData;

        public static FakeData Load()
        {
            var instance = Resources.Load<FakeDataHolder>("FactoryFakeData");
            return instance != null ? instance.FactoryData : null;
        }
    }
}