﻿using Breakthrough.State;
using System;
using UnityEngine;

namespace Breakthrough.Data
{
    public class Sector : MonoBehaviour
    {
        public string Id;
        public GameObject WorkMarker;
        public GameObject RouteMarker;
        public GameObject StorageMarker;

        private GameObject _marker;

        public void Show(PartStateType state)
        {
            _marker = WorkMarker;

            WorkMarker.SetActive(false);
            RouteMarker.SetActive(false);
            StorageMarker.SetActive(false);

            switch (state)
            {
                case PartStateType.Wait:
                    break;
                case PartStateType.Work:
                    _marker = WorkMarker;
                    break;
                case PartStateType.Route:
                    _marker = RouteMarker;
                    break;
                case PartStateType.Storage:
                    _marker = StorageMarker;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }

            _marker.SetActive(true);
        }
    }
}