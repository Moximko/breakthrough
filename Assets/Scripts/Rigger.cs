﻿using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Rigger : MonoBehaviour
{
    LineRenderer route;
    NavMeshAgent agent;
    [SerializeField]
    NavMeshPath rightPath;
    public GameObject cameraBone;
    
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        route = GetComponent<LineRenderer>();
        UI.instance.onClick += OnClick;
        UI.instance.onCameraChange += OnCameraChange;
    }
    void OnClick(Vector3 position)
    {
        agent.destination = new Vector3(position.x, 0, position.z);
    }
    void Update()
    {
        route.positionCount = agent.path.corners.Length;
        route.SetPositions(agent.path.corners.Select(c => new Vector3(c.x, 1, c.z)).ToArray());
    }
    void OnCameraChange(CameraManager.CameraRegime regime)
    {
        route.enabled = regime != CameraManager.CameraRegime.Top;
    }
}
