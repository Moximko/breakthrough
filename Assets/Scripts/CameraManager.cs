﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{  
    Vector3 initialPosition;
    Quaternion initialRotation;
    bool keepRotation;

    public enum CameraRegime
    {
        Top, FirstPerson, ThirdPerson
    }

    void Start()
    {
        initialPosition = transform.position;
        initialRotation = transform.rotation;
        UI.instance.onCameraChange += OnCameraChanged;
    }
    void Update()
    {
        if (keepRotation)
        {
            transform.rotation = initialRotation;
        }
    }

    void OnCameraChanged(CameraRegime regime)
    {   
        keepRotation = false;
        switch (regime)
        {
        case CameraRegime.Top:
            transform.SetParent(null);
            transform.position = initialPosition;
            transform.rotation = initialRotation;
        break;
        case CameraRegime.FirstPerson:
        {
            var rigger = FindObjectOfType<Rigger>();
            transform.SetParent(rigger.cameraBone.transform);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        } break;
        case CameraRegime.ThirdPerson:
        {
            var rigger = FindObjectOfType<Rigger>();
            transform.SetParent(rigger.transform, regime == CameraRegime.ThirdPerson);
            transform.localPosition
                = new Vector3(0, initialPosition.y - rigger.transform.position.y, 0);
            keepRotation = true;
        } break;
        }
    }
}
