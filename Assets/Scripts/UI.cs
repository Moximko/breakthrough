﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
    [SerializeField]
    Dropdown cameraTypes;
    [SerializeField]
    Image riggerIconPrefab;
    Rigger rigger;
    Image riggerIcon;

    static UI instance_;
    public static UI instance {
        get { return instance_; }
        private set { instance_ = value; }
    }

    public event Action<Vector3> onClick;
    public event Action<CameraManager.CameraRegime> onCameraChange;

    public void OnCameraChange()
    {
        onCameraChange((CameraManager.CameraRegime)cameraTypes.value);
    }
    public void OnClick()
    {
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            onClick(hit.point);
        }
    }
    public void OnNextScene()
    {
        SceneManager.LoadScene("MainScene");
    }
    void Awake()
    {
        instance_ = this;
    }
    void Start()
    {
        rigger = FindObjectOfType<Rigger>();
        riggerIcon = GameObject.Instantiate(riggerIconPrefab);
        riggerIcon.transform.SetParent(transform);
    }
    void Update()
    {
        riggerIcon.transform.position
            = Camera.main.WorldToScreenPoint(rigger.transform.position);
    }
    void OnDestroy()
    {
        onClick = null;
        onCameraChange = null;
        instance_ = null;    
    }
}
